import uuid
import time
from multiprocessing import Queue, Event
from threading import Thread, RLock
from utils import lock_while_running, singletone
from logger import LocalAgentLogger


class IPCMessage(object):
    def __init__(self, method, params, sender=None, request_id=None):
        self.method = method
        self.params = params
        self.sender = sender
        self.request_id = request_id

    def __str__(self):
        return 'method: {}, sender: {}, params: {}'.format(
            self.method,
            self.sender,
            self.params,
        )


class Response(object):
    def __init__(self, request_id, result=None, exception=None):
        self.request_id = request_id
        self.result = result
        self.exception = exception


class RPCActor(object):
    def __init__(self, actor_address, obj, results_queue):
        self.__logger = LocalAgentLogger('Actor ({})'.format(actor_address))
        self.__actor_address = actor_address
        self.__continue_to_listen = Event()
        self.__poison_pill = uuid.uuid4()
        self.__results_queue = results_queue
        self.__obj = obj
        # self.__obj.set_actor_address(self.__actor_address)
        self.__queue = Queue()
        self.__message_dispatcher = Thread(target=self.__dispatch_message)
        self.__message_dispatcher.start()

    def __dispatch_message(self):
        while True:
            if self.__continue_to_listen.is_set():
                self.__queue.cancel_join_thread()
                break

            msg = self.__queue.get()

            if msg == self.__poison_pill:
                self.__queue.cancel_join_thread()
                break

            response = Response(request_id=msg.request_id)

            try:
                response.result = getattr(self.__obj, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('BAD MESSAGE: {} ({})'.format(msg, e))
                response.exception = e
            finally:
                if msg.request_id:
                    self.__results_queue.put(response)

    def kill(self):
        self.__continue_to_listen.set()
        self.__queue.put(self.__poison_pill)
        self.__message_dispatcher.join()

        try:
            del self.__obj
        except Exception as e:
            self.__logger.exception('CANNOT DELETE NESTED OBJ: {} ({})'.format(self.__obj, e))

    @property
    def queue(self):
        return self.__queue


@singletone
class System(object):
    def __init__(self):
        self.lock = RLock()
        self.__actors = dict()
        self.__listeners = list()
        self.__logger = LocalAgentLogger('SYSTEM')

    @lock_while_running
    def __get_new_unique_id(self, collection):
        while True:
            new_id = uuid.uuid4()

            if new_id not in collection:
                return new_id

    def tell(self, actor_address, method, params, sender=None):
        if actor_address not in self.__actors:
            raise ValueError('Wrong address: {}'.format(actor_address))

        self.__actors[actor_address]['actor'].queue.put(
            IPCMessage(
                method=method,
                params=params,
                sender=sender
            )
        )

    def ask(self, actor_address, method, params):
        actor_node = self.__actors[actor_address]
        request_id = self.__get_new_unique_id(collection=actor_node['results'])
        actor_node['results'][request_id] = {}
        results_node = actor_node['results'][request_id]
        results_node['blocker'] = Event()

        actor_node['actor'].queue.put(
            IPCMessage(
                method=method,
                params=params,
                sender=None,
                request_id=request_id
            )
        )

        while not results_node['blocker'].is_set():
            time.sleep(0.001)

        result = actor_node['results'].pop(request_id)['result']

        if result.exception:
            raise result.exception
        else:
            return result.result

    @lock_while_running
    def add_actor(self, obj):
        actor_address = self.__get_new_unique_id(collection=self.__actors)
        results_queue = Queue()
        self.__actors[actor_address] = {
            'actor': RPCActor(actor_address, obj, results_queue=results_queue),
            'results': {},
            'continue_to_listen': Event(),
            'poison_pill': uuid.uuid4(),
            'results_queue': results_queue
        }
        Thread(target=self.__get_results_from_actor, args=(actor_address,)).start()
        return actor_address

    def __get_results_from_actor(self, actor_address):
        actor_node = self.__actors[actor_address]
        queue = actor_node['results_queue']

        while True:
            if actor_node['continue_to_listen'].is_set():
                queue.cancel_join_thread()
                break

            msg = queue.get()

            if msg == actor_node['poison_pill']:
                queue.cancel_join_thread()
                break

            actor_node['results'][msg.request_id]['result'] = msg
            actor_node['results'][msg.request_id]['blocker'].set()

    @lock_while_running
    def stop_actor(self, actor_address):
        if actor_address not in self.__actors:
            raise ValueError('Wrong address: {}'.format(actor_address))

        actor_node = self.__actors[actor_address]
        actor_node['continue_to_listen'].set()
        actor_node['results_queue'].put(actor_node['poison_pill'])
        actor_node['actor'].kill()

    @lock_while_running
    def stop_system(self):
        for actor_id in self.__actors:
            self.stop_actor(actor_id)


def main():
    system = System()

    class A(object):
        def __init__(self, a):
            self.a = a

        def get_value_a(self, *args):
            return self.a

        def set_value_a(self, a):
            self.a = a

    a_val = 15
    a_obj = A(a_val)
    a_address = system.add_actor(a_obj)

    system.tell(a_address, 'set_value_a', a_val)
    assert a_obj.get_value_a() == a_val

    for i in range(10):
        system.tell(a_address, 'set_value_a', i)
        time.sleep(0.0001)
        assert system.ask(a_address, 'get_value_a', None) == i

    system.stop_system()


if __name__ == '__main__':
    main()
