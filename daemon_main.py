# todo: Logger output to round-robbin file - Highly important!
# todo: SSL-connections
# todo: rewrite this shit using Actors model (thespian) or similar
import yaml
from logger import LocalAgentLogger
from multiprocessing import Queue
from gui.web import local_gui_web_server
from threading import Thread
from utils import send_ipc_message
from wsclient import ReconnectingBackendWebsocket
from device_manager import DeviceManager
from yaml import CLoader


DEV = False


class GUIStateUpdater(object):
    def __init__(self, webgui_transmitter):
        self.__webgui_transmitter = webgui_transmitter

        self._local_config = None
        self._remote_config = None
        self._devices = []
        self._be_websocket_state = False
        self._attached_com_ports = list()
        self._main_gui_connected = False
        self._backend_response = None
        self._backend_available = False

        # Initial GUI update
        self.push_full_gui_update()

    def push_full_gui_update(self):
        msg = {
            'action': 'updateState',
            'variables': {
                'localConfig': self._local_config,
                'remoteConfig': self._remote_config,
                'beWebsocketState': self._be_websocket_state,
                'attachedComPorts': self._attached_com_ports,
                'mainGuiConnected': self._main_gui_connected,
                'backendResponse': self._backend_response,
                'backendAvailable': self._backend_available,
                'devices': self._devices
            }
        }
        self.push_gui_update(msg)

    @staticmethod
    def __to_camelcase(name):
        parts = name.strip('_').split('_')
        ret = []

        for i, part in enumerate(parts):
            if i == 0:
                ret.append(part.lower())
            else:
                ret.append(part.capitalize())

        return ''.join(ret)

    def push_gui_update(self, msg):
        self.__webgui_transmitter.put(msg)

    def update_state(self, var_name, value):
        if getattr(self, var_name) == value:
            return

        setattr(self, var_name, value)
        self.push_gui_update({
            'action': 'updateState',
            'variables': {
                self.__to_camelcase(var_name): value
            }
        })

    def alert(self, msg):
        self.__webgui_transmitter.put({
            'action': 'alert',
            'message': msg
        })


class Daemon(object):
    """
    Run all required background processes:
    * COM-port monitors
    * light Web server for GUI (local definitions only, such as license hash, external connection port etc.
    * Websocket for interacting with an Operator (using back-end server)
    """
    def __init__(self):
        self.__MAX_QUE_SIZE = 100
        self.__logger = LocalAgentLogger(self.__class__.__name__)
        self.__config_file_path = 'config.yaml'
        self.__local_config = self.__read_local_config()
        self.__remote_config = None
        self.__local_webgui_receiver = Queue(maxsize=self.__MAX_QUE_SIZE)
        self.__local_webgui_transmitter = Queue(maxsize=self.__MAX_QUE_SIZE)
        self.__state = GUIStateUpdater(self.__local_webgui_transmitter)
        self.__state.update_state('_local_config', self.__local_config)
        local_gui_web_server.run_web_server(
            self.__local_webgui_transmitter,
            self.__local_webgui_receiver
        )
        self.__be_websocket_receiver = Queue(maxsize=self.__MAX_QUE_SIZE)
        self.__be_websocket_transmitter = Queue(maxsize=self.__MAX_QUE_SIZE)
        self.__bewebsocket = ReconnectingBackendWebsocket(
            self.__be_websocket_transmitter,
            self.__be_websocket_receiver,
            self.__local_config
        )
        self.__device_manager_receiver = Queue(maxsize=self.__MAX_QUE_SIZE)
        self.__device_manager_transmitter = Queue(maxsize=self.__MAX_QUE_SIZE)
        # self.__device_manager = DeviceManager(
        #     self.__device_manager_transmitter,
        #     self.__device_manager_receiver,
        #     self.__local_config
        # )
        receivers = (
            self.__local_webgui_receiver,
            self.__be_websocket_receiver,
            self.__device_manager_receiver
        )

        for receiver in receivers:
            Thread(target=self.__dispatch_message, args=(receiver,)).start()

    def __dispatch_message(self, queue):
        while True:
            msg = queue.get()

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('BAD COMMAND: {}, ({})'.format(msg.to_dict(), e))

    def __read_local_config(self):
        with open(self.__config_file_path, 'r') as _:
            return yaml.load(_, Loader=CLoader)

    def send_to_be(self, params):
        send_ipc_message(
            queue=self.__be_websocket_transmitter,
            method='send_to_backend',
            params=params
        )

    def send_to_device(self, msg):
        send_ipc_message(
            queue=self.__device_manager_transmitter,
            method='send_to_device',
            params=msg
        )

    def update_state(self, params):
        self.__state.update_state(**params)

    def update_config(self, new_config):
        # Ignore update that updates nothing
        if self.__local_config == new_config:
            return

        self.__local_config = new_config

        with open(self.__config_file_path, 'w') as stream:
            yaml.dump(self.__local_config, stream, default_flow_style=False)

        self.__state.update_state('_local_config', self.__local_config)
        self.__bewebsocket.update_config(self.__local_config)
        self.__state.alert('The configuration has been updated')

    def refresh_gui(self, *args):
        self.__state.push_full_gui_update()


def main():
    """
    The main daemon loop. Should complete the following tasks:
    1. Read config data
    2. Update the Backend with that config data: ports info - baudrate, monitor(True/False), etc.
    3. Should provide its internal IP to the Backend (for running local Websocket server)
    4. Run the local GUI (HTTP server) for user interacting (either by CLI or Web GUI)
    5. Open all the COM-ports (configured as monitor=TRUE)
    """
    Daemon()


if __name__ == '__main__':
    main()
