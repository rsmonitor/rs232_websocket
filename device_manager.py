# todo: Finish DeviceManager
from logger import LocalAgentLogger
from multiprocessing import Queue, Event
import os
import time
from threading import Thread
from serial_handler import SerialHandlerSimulator
from utils import send_ipc_message
from uuid import uuid4
import json
import requests
from requests.exceptions import RequestException


LOAD_TEST = True


class DeviceManager(object):
    def __init__(self, receiver, transmitter, config):
        self.__local_config = config
        self.__logger = LocalAgentLogger('DeviceManager')
        self.__daemon_receiver = receiver
        self.__daemon_transmitter = transmitter
        Thread(target=self.__listen_to_daemon).start()

        self.__configuration = None
        self.__devices = list()
        self.__ports = dict()

        self.__poison_pill_daemon = uuid4()
        self.__continue_to_listen = Event()

        self.__device_scanner = Thread(target=self.__scan_devices)
        self.__device_scanner.start()

        self.__devices_rx_queue = Queue()
        self.__device_listener = Thread(target=self.__listen_to_devices)
        self.__device_listener.start()

        self.__devices_transmitters = dict()
        self.__init_devices()

    def __update_daemon_status(self, method, params):
        send_ipc_message(self.__daemon_transmitter, method, params)

    def __get_settings_from_be(self):
        """
        Get user config data (ports' configs and names, station name, etc.) from the Backend
        """
        be_config = self.__local_config['Backend_server']
        backend_uri_tmpl = 'http://{host}:{port}/{app_name}/get_user_settings/'
        backend_uri = backend_uri_tmpl.format(
            host=be_config['backend_url'],
            port=be_config['backend_port'],
            app_name=be_config['app_name']
        )
        params = {
            'station':  be_config['station'],
            'group':    be_config['group'],
            'license':  json.dumps(be_config['license'])
        }

        # todo: continue endless only if both no server response and no Internet connection
        self.__logger.debug('TRYING TO GET CONFIG FROM BE')
        try:
            req = requests.get(backend_uri, params=params)
            self.__update_daemon_status('update_backend_available', True)

            if req.ok:
                self.__logger.debug('Got config from remote: {}'.format(req.json()))
                self.__remote_config = req.json()
                self.__update_daemon_status('update_backend_response', {'status': True})
            else:
                self.__logger.info('CANNOT GET REMOTE CONFIG: {} ({})'.format(req.status_code, req.reason))
                self.__update_daemon_status(
                    'update_backend_response',
                    {
                        'status': False,
                        'code': req.status_code,
                        'reason': req.reason
                    }
                )
        except (RequestException, ValueError) as e:
            self.__logger.exception('BackEnd unavailable{}'.format(e))
            self.__update_daemon_status('update_backend_available', False)
        finally:
            self.__update_daemon_status('push_gui_update', None)

    def __listen_to_devices(self):
        while not self.__continue_to_listen.is_set():
            msg = self.__devices_rx_queue.get()

            if msg == self.__poison_pill:
                break

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('Wrong message from device: {}'.format(msg, e))

    def __listen_to_daemon(self):
        while True:
            msg = self.__daemon_receiver.get()

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('Bad command from daemon: {}, {}'.format(msg, e))

    def __scan_devices(self):
        if LOAD_TEST:
            self.__devices = {'/dev/ttyUSB0', '/dev/ttyUSB1', '/dev/ttyUSB2'}
            return

        while True:
            new_devices = set(['/dev/' + d for d in os.listdir('/dev') if d.startswith('ttyUSB')])

            if new_devices != self.__devices:
                self.__devices = new_devices
                self.get_status(None)
            time.sleep(0.1)

    def __init_devices(self):
        # Wait until some COM-port connected to the system
        while not self.__devices:
            time.sleep(0.01)

        for device_id in self.__configuration:
            if device_id in self.__devices:
                transmitter = Queue()
                self.__devices_transmitters[device_id] = transmitter
                self.__ports[device_id] = SerialHandlerSimulator(
                    device_id,
                    transmitter,
                    self.__devices_rx_queue,
                    self.__configuration[device_id]
                )

    def update_configuration(self, new_config):
        if self.__configuration == new_config:
            return

        self.__configuration = new_config

    def get_status(self, msg):
        send_ipc_message(
            queue=self.__daemon_transmitter,
            method='push_gui_update',
            params={
                'action': 'updateState',
                'variable': 'devices',
                'data': sorted(list(self.__devices))
            }
        )

    def send_to_device(self, msg):
        device_queue = self.__devices_transmitters[msg['device_id']]
        send_ipc_message(
            queue=device_queue,
            method='send_command',
            params=msg['command']
        )

    def send_to_be(self, msg):
        send_ipc_message(
            queue=self.__daemon_transmitter,
            method='send_to_be',
            params=msg
        )

    def __del__(self):
        self.__continue_to_listen.set()
        self.__daemon_receiver.put(self.__poison_pill)
        self.__devices_rx_queue.put(self.__poison_pill)

        for device_id, serial_handler in self.__ports.items():
            serial_handler.close()
