import React from 'react';

function Cell(props) {
    function handleClick() {
        props.handleClick(props.employee, props.day, props.workShift);
    }

    let caption = ["X", ":\\", "+"][props.status];

    if (props.day === props.badDay && props.workShift === props.badShift) {
        return(
            <div>
                <button onClick={handleClick}><strong>{caption}</strong></button>
            </div>
        )
    }
    else {
        return(
            <div>
                <button onClick={handleClick}>{caption}</button>
            </div>
        )
    }


}

export default Cell;
