import React from 'react';
import Cell from './cell';


function EmployeeTable (props) {
    const employee = props.employee;
    const rows = Array();
    const tableHeader = Array();

    for (let day in props.employeeData) {
            tableHeader.push(
                <td key={day}>
                    <strong>{day.slice(0, 3)}</strong>
                </td>
            )
        }
    rows.push(tableHeader);

    for (let shift in props.employeeData.Sunday) {
        let dayShifts = Array();

        for (let day in props.employeeData) {
            dayShifts.push(
                <td key={day}>
                    <Cell
                        employee={employee}
                        day={day}
                        workShift={shift}
                        handleClick={props.handleClick}
                        status={props.employeeData[day][shift]}
                        badDay={props.badDay}
                        badShift={props.badShift}
                    />
                </td>
            )
        }

        rows.push(
            <tr key={shift}>
                {dayShifts}
            </tr>
        )
    }

    function updateNumberOfDays(e) {
        props.updateNumberOfDays(e.target.value, employee);
    }

    return (
        <div>
            <h3>
                {props.employee}
            </h3>

            <select value={props.numOfDays} onChange={updateNumberOfDays}>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <table>
                <tbody>
                    {rows}
                </tbody>
                </table>
        </div>
    )
}

export default EmployeeTable;
