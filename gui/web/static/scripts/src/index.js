import React from 'react';
import ReactDOM from 'react-dom';
import LocalGUI from "./local-gui";
// import ShiftManager from "./shift-manager";


ReactDOM.render(
    <LocalGUI/>,
    document.getElementById('app')
);
