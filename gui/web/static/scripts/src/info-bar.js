import React from "react";

function InfoBar(props) {
    console.log(props);
    if (!props.localGuiWebServer) {
        return(
            <div id={"info-bar"}>
                <h4>The local Agent seems not working</h4>
                <h5>Check list of processes</h5>
            </div>
        );
    }
    else {
        let devices = Array();
        if (props.devices.length > 0) {
            for (let i in props.devices) {
                let device = props.devices[i];
                devices.push(
                    <li key={device}>
                        <strong>{device}</strong>
                    </li>
                )
            }
        }
        else {
            devices = 'No devices attached to the system';
        }

        let beWSConnected = 'Connected';
        if (props.beWebsocketState.status === false) {
            beWSConnected = "Disconnected";
            if (props.beWebsocketState.code) {
                beWSConnected += `, (code=${props.beWebsocketState.code}`;
            }

            if (props.beWebsocketState.reason) {
                beWSConnected += `, reason=${props.beWebsocketState.reason})`;
            }
        }

        return(
            <div id={"info-bar"}>
                <h3>InfoBar</h3>
                <hr />
                <h5>COM-ports available:</h5>
                <ul>
                    {devices}
                </ul>
                <hr />
                <h5>Backend WebSocket:</h5>{beWSConnected}
            </div>
        )
    }
}

export default InfoBar;
