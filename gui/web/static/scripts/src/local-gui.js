import React from "react";
import SettingsForm from "./settings-form";
import InfoBar from "./info-bar";
import {getDataFromServer, submitData} from "./api";
import ReconnectingWebSocket from 'reconnecting-websocket'


function createWebSocket (url, port, onMessageCallback, setLocalGUIWebServer) {
    const webSocket = new ReconnectingWebSocket(`ws://${url}:${port}/websocket`);
    webSocket.onopen = (status) => {
        //todo: Ask the backend for status updates
        setLocalGUIWebServer(true, status);
    };

    webSocket.onclose = (data) => {
        setLocalGUIWebServer(false);
    };

    webSocket.onmessage = (msg) => {
        onMessageCallback(msg);
    };
    return webSocket;

}

class LocalGUI extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backendAvailable: false,
            beWebsocketState: {status: false},
            devices: Array(),
            attachedComPorts: Array(),
            configData: undefined,
            localGuiWebServer: false,
            localConfig: undefined,
        };
        this.processWebSocketMessage = this.processWebSocketMessage.bind(this);
        this.postUserData = this.postUserData.bind(this);
        this.setLocalGUIWebServer = this.setLocalGUIWebServer.bind(this);
        this.updateConfigData = this.updateConfigData.bind(this);
    }

    setLocalGUIWebServer(status) {
        this.setState({
            localGuiWebServer: status
        });
    }

    processWebSocketMessage(msg) {
        const data = JSON.parse(msg.data);
        // console.log(data);
        switch (data.action) {
            case 'updateState':
                this.setState(data.variables);
                break;
            case 'alert':
                alert(data.message);
                break;
        }
    }

    updateConfigData(data) {
        this.setState({
            configData: data
        });
    }

    getConfig() {
        getDataFromServer("/config", this.updateConfigData, console.log);
    }

    componentDidMount() {
        createWebSocket(
            window.location.hostname,
            window.location.port,
            this.processWebSocketMessage,
            this.setLocalGUIWebServer
        );
        this.getConfig();
    }

    postUserData(data) {
        if (JSON.stringify(data.formData) === JSON.stringify(this.state.localConfig)) {
            alert("Data has not been altered.\nNo submission required");
        }
        else {
            submitData(
                "/config",
                data.formData,
                this.updateConfigData
            );
        }
    }

    render() {
        console.log("I'm alive!!!!!")
        return (
            <div>
                <InfoBar
                    devices={this.state.devices}
                    beWebsocketState={this.state.beWebsocketState}
                    attachedComPorts={this.state.attachedComPorts}
                    localGuiWebServer={this.state.localGuiWebServer}
                />
                <hr />
                <SettingsForm
                    configData={this.state.localConfig}
                    postUserData={this.postUserData}
                />
            </div>
        );
    }
}

export default LocalGUI;
