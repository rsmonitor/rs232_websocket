//todo: Separate schema for a COM-Port definitions and link to it (for dynamic adding and removing a COM-Ports)
//todo: add websocket for immediate update if a new device connected to the system (ttyUSB1)
//todo: Indentations in the forms
//todo: ReactJS router (SPA): minimizing sub-sections (not to overload the page with too many details)
//todo:
/*
** Local GUI should use ReconnectingWebSocket (as in MainGUI)
** GUI connected/disconnected (browser closed/opened)
** A new COM-port appears in the system (if no ports in the system - inform local GUI too)
** An existing COM-port disappeared/closed
** Settings of a specific port has changed in the MainGUI - reopen an appropriate COM-port with the new settings
** Open/Close toggled on MainGUI - (inform local GUI)
** BackEnd unavailable/disconnect - update local GUI
** Wrong license - update local GUI
*/

import React from "react";
import Form from "react-jsonschema-form";

class SettingsForm extends React.Component {
    constructor(props) {
        super(props);
        this.standardTypes = ["string", "number", "boolean"];
        this.state = {
            configData: props.configData,
            postUserData: props.postUserData
        };
        this.updateUserInfo = this.updateUserInfo.bind(this);
    }

    componentWillReceiveProps(newProps) {
        if (this.state.configData !== newProps.configData) {
            this.setState({
                configData: newProps.configData
            });
        }
    }

    shouldComponentUpdate(newProps, newState) {
        return newProps.configData !== this.state.configData;
    }

    generateSchema(data, title) {
        const dataType = typeof data;
        let schema = {
            title: title,
            type: dataType
        };

        if (this.standardTypes.indexOf(dataType) >= 0) {
            schema.default = data;
        }
        else if (dataType === "object") {
            schema.properties = {};
            for (let property in data) {
                schema.properties[property] = this.generateSchema(data[property], property);
            }
        }
        else {
            console.log(`UNKNOWN TYPE: ${dataType}\n`);
        }

        return schema;
    }

    generateUISchema(data, title) {
        const dataType = typeof data;
        let schema = {};
        if (this.standardTypes.indexOf(dataType) >= 0) {
            if (title === "password") {
                schema["ui:widget"] = "password";
            }
            else if (title === "email") {
                schema["ui:widget"] = "email";
            }
            else if (title === "license") {
                schema["ui:widget"] = "textarea";
            }
        }
        else if (dataType === "object") {
            for (let property in data) {
                schema[property] = this.generateUISchema(data[property], property);
            }
        }
        else {
            console.log(`UNKNOWN TYPE: ${dataType}\n`);
        }
        return schema;
    }

    updateUserInfo(e) {
        // console.log(e.formData);
    }


    render() {
        if (!this.state.configData) {
            return (
                <div>No config data...</div>
            )
        }
        else {
            return (
                <Form
                    schema={this.generateSchema(this.state.configData)}
                    uiSchema={this.generateUISchema(this.state.configData, "Config")}
                    formData={this.state.configData}
                    onSubmit={this.state.postUserData}
                    onChange={this.updateUserInfo}
                />
            );
        }
    }
}

export default SettingsForm;
