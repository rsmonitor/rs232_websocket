# todo: remove ":root:" in the beginning of string
# todo: Limit number of lines in output (round-robbin)
import logging
from logging import FileHandler, StreamHandler
from datetime import datetime
import traceback


DEV = True


class LocalAgentLogger(object):
    def __init__(self, name):
        self.__name = name
        if DEV:
            divider = '\n' + '*' * 40
            self.__msg_template = divider + '\n{time_stamp}\n{name}\n{msg}\n' + divider + '\n\n\n'
        else:
            self.__msg_template = '{time_stamp}\t{name}\t{msg}'
        self.__handler = None

        if DEV:
            self.__handler = StreamHandler()
            logging.basicConfig(level=logging.DEBUG)
        else:
            self.__handler = FileHandler('log.txt')
            logging.basicConfig()

    def __print_msg(self, msg, level):
        t = datetime.now()
        time_stamp = '{}({}:{}:{}.{})'.format(
            t.date(), t.hour, t.minute, t.second, str(t.microsecond)[:2]
        )

        getattr(logging, level)(
            self.__msg_template.format(
                time_stamp=time_stamp,
                name=self.__name,
                msg=msg
            )
        )

    def debug(self, msg):
        self.__print_msg(msg=msg, level='debug')

    def info(self, msg):
        self.__print_msg(msg, level='info')

    def critical(self, msg):
        self.__print_msg(msg, level='critical')

    def warning(self, msg):
        self.__print_msg(msg, level='warning')

    def exception(self, msg, traceback_obj=None):
        self.__print_msg(msg, level='exception')
        print(traceback_obj)
