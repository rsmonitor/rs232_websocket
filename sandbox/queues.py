from multiprocessing import Queue
from threading import Thread, RLock
from contextlib import contextmanager
from random import seed, shuffle
import time

returned_data = []
ask_for_execution_order = []


# @contextmanager
# def single_thread_executor(lock):
#     try:
#         print('\nHello world!\n')
#         yield lock.acquire()
#     except Exception as e:
#         print('Exception: {}'.format(e))
#     finally:
#         lock.release()
#
#
# def lock_while_running(f):
#     def wrapper(self, *args, **kwargs):
#         ask_for_execution_order.append(args[0])
#
#         with single_thread_executor(self.lock):
#             return f(self, *args, **kwargs)
#
#     return wrapper

q = Queue()


def do(i):
    ask_for_execution_order.append(i)
    time.sleep(0.1)
    q.put(i)


def main():
    threads = []
    for i in range(100):
        threads.append(Thread(target=do, args=(i,)))

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    while not q.empty():
        returned_data.append(q.get())
    # print(returned_data == ask_for_execution_order)
    for i in range(len(returned_data)):
        print(returned_data[i], ask_for_execution_order[i])


if __name__ == '__main__':
    main()
