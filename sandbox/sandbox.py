import uuid
from logger import LocalAgentLogger
from multiprocessing import Queue, Event
from threading import Thread


class IPCMessage(object):
    def __init__(self, method, params):
        self.method = method
        self.params = params

    def __str__(self):
        return 'method: {}\nparams: {}'.format(self.method, self.params)


class RPC(object):
    def __init__(self, incoming):
        self.__incoming = incoming
        self.__logger = LocalAgentLogger(self.__str__())
        self.__continue_listening = Event()
        self.__poison_pill = uuid.uuid4()
        self.__listener = Thread(target=self.__dispatch_message)
        self.__listener.start()

    def __dispatch_message(self):
        while True:
            if self.__continue_listening.is_set():
                self.__incoming.cancel_join_thread()
                break

            msg = self.__incoming.get()

            if msg == self.__poison_pill:
                self.__incoming.cancel_join_thread()
                break

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('BAD COMMAND: {}'.format(e))

    def kill(self, params):
        self.__continue_listening.set()
        self.__incoming.put(self.__poison_pill)

        try:
            self.__listener.join(timeout=0.1)
        except Exception as e:
            self.__logger.exception('CANNOT JOIN THE LISTENER: {}'.format(e))
