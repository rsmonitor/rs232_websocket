def to_camelcase(name):
    parts = name.strip('_').split('_')
    ret = []

    for i, part in enumerate(parts):
        if i == 0:
            ret.append(part.lower())
        else:
            ret.append(part.capitalize())

    return ''.join(ret)


print(to_camelcase('device'))
