"""
The module provides basic thread-safe full-duplex SerialHandler (and Simulator) for COM-port interoperability.
Lets
"""
import time
import uuid
from serial.serialposix import Serial, SerialException
from threading import RLock, Thread, Event
from utils import lock_while_running
from logger import LocalAgentLogger


class SerialHandler(object):
    def __init__(self, device_id, baudrate=115200, parity='N', timeout=1.0):
        """
        @param device_id: Either device id in the OS or COM-port of the device
        @type device_id: str
        @param baudrate: data transfer speed, [baud]
        @type baudrate: int
        """
        self.__logger = LocalAgentLogger('SerialHandler')
        self.__device_id = device_id
        self.__baudrate = baudrate
        self.__parity = parity
        self.__buffer_length = 16
        self.__timeout = 5.0
        self.__lock = RLock()
        self.__port = None
        self.__serial = Serial(device_id, baudrate=baudrate, timeout=timeout, parity=parity)
        retries_to_open = 10

        while not self.__serial.isOpen():
            try:
                self.__serial.open()
            except:
                pass

            time.sleep(0.1)
            retries_to_open -= 1

            if retries_to_open <= 0:
                SerialException('Can not open the port {}'.format(device_id))

    def read_all_from_buffer(self):
        return self.__serial.read(self.__serial.inWaiting)

    def is_open(self):
        if self.__serial:
            return self.__serial.isOpen()

        return False

    def read_all(self):
        return self.__serial.read_all()

    def send_command(self, command):
        self.__logger.debug('Sending a {} command to the COM-Port'.format(command))
        return self.__serial.write(command)

    def get_response(self, pause=0.05):
        time.sleep(pause)
        return self.__serial.read(self.__serial.inWaiting())

    def open_port(self):
        if not self.__serial.is_open:
            self.__serial.open()
            return self.__serial.is_open

    def close_port(self):
        if self.__serial:
            self.__serial.close()
            self.__serial = None
            self.__port = None
            return True

        return False

    def disconnect(self):
        if self.is_open():
            self.flush_buffer()
            self.close_port()

    ##########################################################################################
    ########################## Legacy code. Probably no need at all ##########################
    ##########################################################################################

    def init_com_res(self, timeout=8.0, rescan=False):
        """Try to init and open the port.
        Useful both when initializing and retry to initialize the COM-port.
        If the COM-port is not functioning, rescan the addresses (with timeout),
        obtain a new address and then try to initialize the port with a new address.

        @param timeout: timeout for init operation, [s]
        @param timeout: float
        @return: None
        @rtype: None
        """
        status = True
        return
        # try:
        #     if self.__port is None:
        #         self.__port = SerialPortsList().get_com_port(device_id=self.__device_id, rescan=rescan)
        #         if self.__port is None:
        #             raise Exception('No port found for {}'.format(self.__device_id))
        #     if self.__serial is None and self.__port:
        #         self.__serial = serial.Serial(port=self.__port, baudrate=self.__baudrate, timeout=self.__timeout, parity=self.__parity)
        #         self.__serial.flush()
        # except Exception as exc:
        #     status = False

    def flush_buffer(self):
        if self.__serial is None:
            raise SerialException('There is no COM-port for {}'.format(self.__device_id))
        self.__serial.flushInput()
        return self.__serial.flushOutput()

    def listen(self, timeout, data_processor=None):
        if self.__serial is None:
            raise SerialException('There is no COM-port for {}'.format(self.__device_id))

        time_0 = time.time()
        port_buffer = str()

        while (time.time() - time_0) < timeout:
            port_buffer += self.__serial.read(self.__serial.inWaiting())

            if data_processor:
                data_processed_result = data_processor(port_buffer)

                if data_processed_result:
                    return data_processed_result

            time.sleep(timeout / 10.0)

        return port_buffer

    def listen_big_data(self, timeout, exit_condition=None):
        if self.__serial is None:
            raise SerialException('There is no COM-port for {}'.format(self.__device_id))

        self.__serial.timeout = 0.5
        time_0 = time.time()
        port_buffer = []

        while (time.time() - time_0) < timeout:
            port_buffer.append(self.__serial.read(4096))

            if exit_condition and bool(len(port_buffer)):
                if exit_condition(port_buffer[len(port_buffer) - 1]):
                    break

        data = "".join(port_buffer)
        self.__serial.timeout = self.__timeout
        return data

    def send_and_receive(self, command, pause=0.1, response_time=0.25):
        self.send_command(command=command)
        time.sleep(response_time)
        return self.get_response(pause=pause)

    # def pulling(self, command, bad_answer, timeout=5, interval=0.1):
    #     start = time.time()
    #
    #     while (time.time() - start) < timeout:
    #         response = self.send_and_receive(command)
    #
    #         if bad_answer in response:
    #             time.sleep(interval)
    #             continue
    #
    #         return response
    #     return 'ERROR'


class SerialHandlerSimulator(object):
    standard_response = 'FWD-2 13.12.2014\nTEST\n$OK\n'

    def __init__(self, device_id, receiver, transmitter, *args, **kwargs):
        self.__logger = LocalAgentLogger(self.__class__.__name__)
        self.__logger.info('SerialHandlerSimulator {} has started'.format(device_id))
        self.__continue_to_listen = Event()
        self.__poison_pill = uuid.uuid4()
        self.__device_id = device_id
        self.__receiver = receiver
        self.__transmitter = transmitter
        self.__open = True
        self.__listen_to_receiver_proc = Thread(target=self.__listen_to_receiver)
        self.__listen_to_receiver_proc.start()

    def __listen_to_receiver(self):
        while not self.__continue_to_listen.is_set():
            msg = self.__receiver.get()

            if msg == self.__poison_pill:
                break

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.exception('Wrong command: {} (Exception: {})'.format(msg.to_dict(), e))

    # def __poll(self):
    #     frame = 'FWD-2 13.12.2014\nTEST\n$OK'
    #
    #     while self.__open:
    #         send_ipc_message(
    #             queue=self.__transmitter,
    #             method='send_com_port_msg_to_be',
    #             params={
    #                 'device_id': self.__device_id,
    #                 'frame': frame,
    #                 'origin': 'SerialHandler'
    #             }
    #         )

    def send_command(self, cmd):
        self.__logger.info('Sending command:{}'.format(cmd))
        # time.sleep(0.001)
        self.__send_device_response(cmd)

    def close(self, *args, **kwargs):
        self.__open = False
        self.__continue_to_listen.set()
        self.__receiver.put(self.__poison_pill)

    def __send_device_response(self, cmd):
        if not self.__open:
            return

        send_ipc_message(
            queue=self.__transmitter,
            method='send_to_be',
            params={
                'device_id': self.__device_id,
                'frame': self.standard_response,
                'sender': 'SerialHandler'
            }
        )
