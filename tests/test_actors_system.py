from actors_system import System
import time

system = System()


class A(object):
    def __init__(self, a):
        self.a = a

    def get_value_a(self, *params):
        return self.a

    def set_value_a(self, a):
        self.a = a


def test_tell():
    a_val = 15
    a_obj = A(a_val)
    a_address = system.add_actor(a_obj)

    system.tell(a_address, 'set_value_a', a_val)
    assert a_obj.get_value_a() == a_val

    for i in range(100):
        system.tell(a_address, 'set_value_a', i)
        time.sleep(0.001)
        assert system.ask(a_address, 'get_value_a', None) == i

    system.stop_system()
