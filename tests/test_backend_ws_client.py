import uuid
from wsclient import run_backend_websocket
from multiprocessing import Queue, Event
from threading import Thread
from websocket_server import WebsocketServer
from utils import send_ipc_message
import json


class WSServer(object):
    def __init__(self, receiver, transmitter, port, host):
        self.__poison_pill = uuid.uuid4()
        self.__continue_to_listen = Event()
        self.__transmitter = transmitter
        self.__receiver = receiver
        self.__clients = []
        Thread(target=self.__dispatch_msg).start()
        self.__server = self.__config_server(port, host)

    @property
    def server(self):
        return self.__server

    @property
    def clients(self):
        return self.__clients

    def __dispatch_msg(self):
        while True:
            if self.__continue_to_listen.is_set():
                self.__receiver.cancel_join_thread()
                return

            msg = self.__receiver.get()

            if msg == self.__poison_pill:
                self.__receiver.cancel_join_thread()
                return

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                print(e)

    def __config_server(self, port, host):
        server = WebsocketServer(port, host)

        def message_received(*args):
            self.__transmitter.put({
                'sender': 'server',
                'data': args[2]
            })

        def client_left(*args, **kwargs):
            self.__clients = server.clients

        def update_list_of_clients(*args, **kwargs):
            self.__clients = server.clients

        server.set_fn_message_received(message_received)
        server.set_fn_client_left(client_left)
        server.set_fn_new_client(update_list_of_clients)
        self.__server_proc = Thread(target=server.run_forever)
        self.__server_proc.start()
        return server

    def send_message(self, client, msg):
        self.__server.send_message(client, msg)

    def shutdown(self):
        self.__continue_to_listen.set()
        self.__receiver.put(self.__poison_pill)

        for client in self.clients:
            client['handler'].send_text('')

        self.__server.shutdown()
        self.__server_proc.join(0.1)


def test_ws_server():
    port = 8002
    host = '127.0.0.1'
    server = WSServer(Queue(), Queue(), port, host)
    server.shutdown()


def test_webscoket_multi_threading():
    port = 8001
    host = '127.0.0.1'
    server_receiver = Queue()
    server_transmitter = Queue()
    server = WSServer(server_transmitter, server_receiver, port, host)

    client_transmitter = Queue()
    client_receiver = Queue()
    station_id = 1
    config = {
        'Backend_server': {
            'backend_url': host,
            'backend_port': port,
            'app_name': 'monitor',
            'station': 'NUC_1200'
        }
    }
    # New websocket client created and connected to the ws server
    Thread(
        target=run_backend_websocket,
        args=(client_transmitter, client_receiver, config, station_id, False)
    ).start()

    # MSG: client reports it is connected
    msg = client_receiver.get()
    assert msg.params['ws_client_connected'] is True

    def sender(arg):
        msg_to_send = {'efi': arg}
        server.send_message(server.clients[0], json.dumps(msg_to_send))
        assert client_receiver.get().params == msg_to_send

    for i in range(100):
        Thread(target=sender, args=(i,)).start()

    server.shutdown()


def test_wsc_connect():
    port = 8003
    host = '127.0.0.1'
    server_receiver = Queue()
    server_transmitter = Queue()
    server = WSServer(server_transmitter, server_receiver, port, host)
    client_receiver = Queue()
    client_transmitter = Queue()

    station_id = 1
    config = {
        'Backend_server': {
            'backend_url': host,
            'backend_port': port,
            'app_name': 'monitor',
            'station': 'NUC_1200'
        }
    }

    Thread(
        target=run_backend_websocket,
        args=(client_transmitter, client_receiver, config, station_id, False)
    ).start()
    # Ensure the client is connected to server
    msg = client_receiver.get()
    assert msg.params['ws_client_connected'] is True

    msg = {'name': 'efi'}
    send_ipc_message(
        queue=client_transmitter,
        method='send_to_backend',
        params=msg
    )
    assert json.loads(server_receiver.get()['data']) == msg
    server.shutdown()
    msg = client_receiver.get()
    assert msg.params['ws_client_connected']['status'] is False
