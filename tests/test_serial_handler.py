from serial_handler import SerialHandlerSimulator, SerialHandler
from multiprocessing import Queue
from threading import Thread
from utils import IPCMessage


def test_send_and_receive_single_thread():
    device_id = '/dev/ttyUSB0'
    rx, tx = Queue(), Queue()
    expected_response = IPCMessage(
        method='send_to_be',
        params={
            'device_id': device_id,
            'frame': SerialHandlerSimulator.standard_response,
            'sender': 'SerialHandler'
        }
    )
    Thread(target=lambda: SerialHandlerSimulator(device_id, tx, rx)).start()
    tx.put(IPCMessage(method='send_command', params='IDN:?;'))
    tx.put(IPCMessage(method='close', params=None))
    assert rx.get().to_dict() == expected_response.to_dict()


def test_send_and_receive_many_threads():
    device_id = '/dev/ttyUSB0'
    rx, tx = Queue(), Queue()
    expected_response = IPCMessage(
        method='send_to_be',
        params={
            'device_id': device_id,
            'frame': SerialHandlerSimulator.standard_response,
            'sender': 'SerialHandler'
        }
    )
    Thread(target=lambda: SerialHandlerSimulator(device_id, tx, rx)).start()

    def talk_to_sh():
        tx.put(IPCMessage(method='send_command', params='IDN:?;'))
        assert rx.get().to_dict() == expected_response.to_dict()

    workers = list()

    for i in range(9999):
        workers.append(Thread(target=talk_to_sh))

    for worker in workers:
        worker.start()

    tx.put(IPCMessage(method='close', params=None))
