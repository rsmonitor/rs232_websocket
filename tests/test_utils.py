from utils import lock_while_running
from threading import Lock, Thread


class ThreadSafeExecutor(object):
    def __init__(self):
        self.lock = Lock()
        self.protected_state = None

    @lock_while_running
    def set_get_protected_state(self, new_state):
        self.protected_state = new_state
        return self.protected_state

    @lock_while_running
    def set_get_protected_state(self, new_state):
        self.protected_state = new_state
        return self.protected_state


def test_lock_while_running():
    tse = ThreadSafeExecutor()
    threads = []

    def runner(param):
        assert tse.set_get_protected_state(param) == param

    for i in range(100):
        threads.append(Thread(target=runner, args=(i,)))

    for t in threads:
        t.start()

    for t in threads:
        t.join()
