from logger import LocalAgentLogger
from contextlib import contextmanager


LOGGER = LocalAgentLogger(__name__)


def empty_queue(queue):
    while queue.qsize():
        queue.get()


def singletone(klass):
    singletone.instances = {}

    def wrapper(*args, **kwargs):
        if klass not in singletone.instances:
            singletone.instances[klass] = klass(*args, **kwargs)

        return singletone.instances[klass]

    return wrapper


@contextmanager
def single_thread_executor(lock):
    logger = LocalAgentLogger('single_thread_execute')

    try:
        yield lock.acquire()
    except Exception as e:
        logger.warning('Failed to acquire a lock: {}'.format(e))
    finally:
        lock.release()


def lock_while_running(f):
    def wrapper(self, *args, **kwargs):
        with single_thread_executor(self.lock):
            return f(self, *args, **kwargs)

    return wrapper


class IPCMessage(object):
    def __init__(self, method, params=None):
        self.method = method
        self.params = params

    def to_dict(self):
        return {
            'method': self.method,
            'params': self.params
        }


def send_ipc_message(queue, method, params=None):
    queue.put(
        IPCMessage(
            method=method,
            params=params
        )
    )

#
# class RemoteProcedureCall(object):
#     def __init__(self, input_queues):
#         self.lock = RLock()
#         self.__logger = LocalAgentLogger(self.__class__.__name__)
#         self.__input_queues = input_queues
#         self.__poison_pill = uuid.uuid4()
#         self.__continue_to_listen = Event()
#
#         for queue in self.__input_queues:
#             listener = Thread(
#                 target=self.__listen_to_input,
#                 args=(queue,)
#             )
#             listener.start()
#
#     def __listen_to_input(self, q):
#         while not self.__continue_to_listen.is_set():
#             msg = q.get()
#
#             if msg == self.__poison_pill:
#                 break
#
#             self.dispatch_msg_to_method(msg)
#
#     @lock_while_running
#     def dispatch_msg_to_method(self, msg):
#         try:
#             getattr(self, msg.method)(msg.params)
#         except Exception as e:
#             self.__logger.warning('BAD MESSAGE: {} (exception: {})'.format(msg, e))
#
#     def stop(self):
#         self.__continue_to_listen.set()
#
#         for q in self.__input_queues:
#             q.put_no_wait(self.__poison_pill)
