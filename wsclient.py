# todo: connect using credentials
import json
import time
import uuid
from threading import Thread, Event
from utils import send_ipc_message
from ws4py.client.threadedclient import WebSocketClient
from logger import LocalAgentLogger
from multiprocessing import Process


class BackendWSClient(WebSocketClient):
    def __init__(self, receiver, transmitter, config, station_id, protocols):
        self.__logger = LocalAgentLogger(self.__class__.__name__)
        be_config = config['Backend_server']
        url = 'ws://{}:{}/ws/{}/{}/'.format(
            be_config['backend_url'],
            be_config['backend_port'],
            be_config['app_name'],
            station_id,
        )
        super().__init__(url, protocols)
        self.station_name = be_config['station']
        self.__logger.debug(be_config)
        self.__receiver = receiver
        self.__transmitter = transmitter
        self.__poison_pill = uuid.uuid4()
        self.__continue_to_listen = Event()
        self.__is_connected = False
        self.__receiver_listener = Thread(target=self.__dispatch_incoming_message)
        self.__receiver_listener.start()

    def __dispatch_incoming_message(self):
        while True:
            if self.__continue_to_listen.is_set():
                self.__receiver.cancel_join_thread()
                break

            msg = self.__receiver.get()

            if msg == self.__poison_pill:
                self.__receiver.cancel_join_thread()
                break

            try:
                getattr(self, msg.method)(msg.params)
            except Exception as e:
                self.__logger.warning('Bad command: {} ({})'.format(msg.to_dict(), e))

    def __update_daemon_state(self, var_name, value):
        send_ipc_message(
            queue=self.__transmitter,
            method='update_state',
            params={
                'var_name': var_name,
                'value': value
            }
        )

    def close(self, code=1000, reason=''):
        self.__continue_to_listen.set()
        self.__receiver.put(self.__poison_pill)
        super().close(code, reason)

    def send_to_backend(self, data):
        if self.__is_connected:
            try:
                d = json.dumps(data)
            except Exception as e:
                self.__logger.exception('FAILED TO JSONIFY DATA: {} (exception: {})'.format(data, e))
            else:
                self.send(d)

    def is_connected(self):
        return self.__is_connected

    def opened(self):
        self.__is_connected = True
        self.__update_daemon_state(
            '_be_websocket_state',
            {
                'status': self.__is_connected,
                'code': None,
                'reason': None
            }
        )

    def closed(self, code, reason=None):
        self.__is_connected = False
        self.__receiver.put(self.__poison_pill)
        self.__update_daemon_state(
            '_be_websocket_state',
            {
                'status': self.__is_connected,
                'code': code,
                'reason': reason
            }
        )
        self.__logger.info("Closed down. Code={}, reason={}".format(code, reason))

    def received_message(self, message):
        data = json.loads(message.data.decode('utf-8'))

        # Ignore echo-messages
        if data.get('sender', None) == 'SerialHandler':
            return

        try:
            send_ipc_message(
                queue=self.__transmitter,
                method='send_to_device',
                params=data
            )
        except Exception as e:
            self.__logger.warning('Failed to send IPC message. Exception: {}'.format(e))


class ReconnectingBackendWebsocket(object):
    def __init__(
        self,
        receiver,
        transmitter,
        config,
        restart_on_fail=True,
        protocols=('http-only', 'chat'),
        refresh=5.0
    ):
        self.__logger = LocalAgentLogger(self.__class__.__name__)
        self.__refresh_period = refresh
        self.__receiver = receiver
        self.__transmitter = transmitter
        self.__config = None
        self.__station_id = None
        self.__restart_on_fail = restart_on_fail
        self.__protocols = protocols
        self.__connect_proc = None
        self.__config = config
        self.__reset_connection()

    def update_station_id(self, station_id):
        if self.__station_id == station_id:
            return

        self.__station_id = station_id
        self.__reset_connection()

    def update_config(self, config):
        if self.__config == config:
            return

        self.__config = config
        self.__reset_connection()

    def __reset_connection(self):
        if self.__connect_proc:
            try:
                send_ipc_message(self.__receiver, 'close')
                self.__connect_proc.terminate()
                self.__connect_proc.join()
            except Exception as e:
                self.__logger.exception(e)

        self.__connect_proc = Process(target=self.__connect)
        self.__connect_proc.start()

    def __connect(self):
        be_websocket = None
        while True:
            try:
                be_websocket = BackendWSClient(
                    self.__receiver, self.__transmitter, self.__config, self.__station_id, self.__protocols
                )
                be_websocket.connect()
            except Exception as e:
                be_websocket.terminate()
                # self.__logger.exception('Cannot run websocket: {}'.format(e))
                send_ipc_message(
                    queue=self.__transmitter,
                    method='update_state',
                    params={
                        'var_name': '_be_websocket_state',
                        'value': {
                            'status':   False,
                            'code':     None,
                            'reason':   e.args
                        }
                    }
                )
                time.sleep(self.__refresh_period)

                if not self.__restart_on_fail:
                    break
            else:
                be_websocket.run_forever()
